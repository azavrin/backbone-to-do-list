describe("Namespace", function () {
  it("provides the 'App' object", function () {
    expect(App).to.be.an("object");
  });
  it("'App' have sub namespaces", function () {
      expect(App).to.include.keys(
        "Collections", "Models", "Templates", "Views"
      );
  });
});
