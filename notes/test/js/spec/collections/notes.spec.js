describe("App.Collections.Notes", function () {
    it("collection is ok", function () {
        expect(new App.Collections.Notes()).is.ok;
    });

    describe("Notes", function () {
        var collection,
            model;

        beforeEach(function () {
            collection = new App.Collections.Notes();
            model = null;
        });

        it("collection is empty by default", function () {
            expect(collection).have.lengthOf(0);
        });

        it("collection can be updated", function () {
            collection.add([
                {
                    cheked: false,
                    text: "some text",
                    createdAt: new Date()
                },
                {
                    cheked: true,
                    text: "one more some text",
                    createdAt: new Date()
                },
            ])
            expect(collection).have.lengthOf(2);
            model = collection.shift();
            expect(collection).have.lengthOf(1);
            expect(model.get("text")).is.equal("some text");
            expect(model.get("createdAt")).is.a("Date");

            model = collection.shift();
            expect(collection).have.lengthOf(0);
            expect(model.get("text")).is.equal("one more some text");
            expect(model.get("createdAt")).is.a("Date");
        });
    });
});
