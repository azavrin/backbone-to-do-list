describe("App.Views.TodoView", function () {

    before(function () {
        this.view = new App.Views.TodoView({
            model: new App.Models.Note({ text: "some text" })
        });
    });

    after(function () {
        this.view.remove();
    });

    it("model text is ok", function () {
        expect(this.view.model.get('text')).is.equal('some text');
    });

    it("remove view on model destroy", sinon.test(function () {
        this.stub(this.view, "remove");
        this.view.model.trigger('destroy');
        expect(this.view.remove).to.be.calledOnce;
    }));

    it("rerender view on model change", sinon.test(function () {
        this.stub(this.view, "render");
        this.view.model.trigger('change');
        expect(this.view.render).to.be.calledOnce;
    }));

    describe("DOM", function () {
        it("renders data to HTML", function () {
            var $item = this.view.render().$el;
            expect($item.find(".list-group-item-text").text()).to.equal("some text");
            expect($item.find(".edit").val()).to.equal("some text");
        });
    });

    describe("actions", function () {
        it("toggle", sinon.test(function () {
            this.stub(this.view, "toggleDone");
            this.view.$(".toggle").click();

            expect(this.view.toggleDone).to.be.calledOnce;
        }));

        it("edit", sinon.test(function () {
            this.stub(this.view, "edit");
            this.view.$(".list-group-item-text").dblclick();

            expect(this.view.edit).to.be.calledOnce;
        }));

        it("destroy", sinon.test(function () {
            this.stub(this.view, "clear");
            this.view.$(".destroy").click();
            expect(this.view.clear).to.be.calledOnce;
        }));

        it("update", sinon.test(function () {
            this.stub(this.view, "updateOnEnter");
            this.view.$(".edit").keypress();

            expect(this.view.updateOnEnter).to.be.calledOnce;
        }));
    });

});
