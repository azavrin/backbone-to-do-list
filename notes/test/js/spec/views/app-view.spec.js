describe("App.Views.AppView", function () {

    beforeEach(function () {
        this.collection = new App.Collections.Notes();
        this.sinonView = new App.Views.AppView({
            collection: this.collection
        });
    });

    after(function () {
        this.sinonView.remove();
    });

    it("add one note event", sinon.test(function () {
        this.stub(this.sinonView, "addOne");
        this.collection.add({ text: "some text" });
        expect(this.sinonView.addOne).to.be.calledOnce;
    }));

    it("add all notes", sinon.test(function () {
        this.stub(this.sinonView, "addAll");
        this.collection.reset();
        expect(this.sinonView.addAll).to.be.calledOnce;
    }));

});
