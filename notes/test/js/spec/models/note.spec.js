describe("App.Models.Note", function () {
  it("check default values", function () {
    // Create empty note model.
    var model = new App.Models.Note();

    expect(model).to.be.ok;
    expect(model.get("text")).to.equal("");
    expect(model.get("createdAt")).to.be.a("Date");
  });

  it("sets passed attributes", function () {
    var model = new App.Models.Note({
      text: "*Sleep\n*Eat\n*Repeat"
    });

    expect(model.get("text")).to.equal("*Sleep\n*Eat\n*Repeat");
  });
});
