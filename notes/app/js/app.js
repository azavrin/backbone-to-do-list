﻿(function (Notes, AppView, Router) {
    app.collection = new Notes();
    
    app.notesView = new AppView({
        collection: app.collection
    });
    
    app.router = new Router();

    Backbone.history.start();
})(App.Collections.Notes, App.Views.AppView, App.Router);