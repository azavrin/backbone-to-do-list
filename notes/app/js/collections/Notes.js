(function (BackboneCollection, Note) {
    var notes = BackboneCollection.extend({
        model: Note,
        url: "/notes"
    });
    $.extend(true, App.Collections, { Notes: notes });
}(Backbone.Collection, App.Models.Note));
