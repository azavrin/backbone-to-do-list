﻿(function (BackboneRouter) {
    router = BackboneRouter.extend({
        routes: {
            "": "notes"
        },
        notes: function () {
            app.notesView.render();
        }
    });
    $.extend(true, App, { Router: router });
})(Backbone.Router);