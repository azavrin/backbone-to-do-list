(function (BackboneModel) {
    var note = BackboneModel.extend({
        defaults: function () {
            return {
                cheked: false,
                text: "",
                createdAt: new Date()
            };
        },
        toggle: function() {
            this.set('cheked', !this.get('cheked'));
        }
    });
    $.extend(true, App.Models, { Note: note });
}(Backbone.Model));
