(function (BackboneView, TodoView, Note) {
    var appView = BackboneView.extend({
        el: $("#app"),
        events: {
            "keypress #new-todo": function (e) { this.createOnEnter(e); }
        },
        initialize: function () {
            this.input = this.$("#new-todo");

            this.listenTo(this.collection, 'add', function (todo) { this.addOne(todo) });
            this.listenTo(this.collection, 'reset', function () { this.addAll() });
        },
        render: function () {
            this.collection.fetch({ reset: true });
        },
        addOne: function (todo) {
            var view = new TodoView({ model: todo });
            this.$("#todo-list").append(view.render().el);
        },
        addAll: function () {
            this.collection.each(this.addOne, this);
        },
        createOnEnter: function (e) {
            if (e.keyCode != 13 || !this.input.val()) return;
            var model = new Note({ text: this.input.val() });
            this.collection.add(model);
            model.save();
            this.input.val('');
        }
    });
    $.extend(true, App.Views, { AppView: appView });
})(Backbone.View, App.Views.TodoView, App.Models.Note);