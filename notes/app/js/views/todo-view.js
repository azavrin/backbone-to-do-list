(function (BackboneView) {
    var todoView = BackboneView.extend({
        tagName: "li",
        template: _.template($('#item-template').html()),
        events: {
            "click .toggle": function () { this.toggleDone() },
            "dblclick .list-group-item-text": function () { this.edit() },
            "click a.destroy": function () { this.clear() },
            "keypress .edit": function (e) { this.updateOnEnter(e); },
            "blur .edit": function () { this.close() }
        },
        attributes: {
            class: "list-group-item"
        },
        initialize: function () {
            this.listenTo(this.model, 'change', function () { this.render() });
            this.listenTo(this.model, 'destroy', function () { this.remove() });
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.input = this.$('.edit');
            return this;
        },
        toggleDone: function () {
            this.model.toggle();
            this.model.save();
        },
        edit: function () {
            this.$el.addClass("editing");
            this.input.focus();
            var strLength = this.input.val().length;
            this.input[0].setSelectionRange(strLength, strLength);
        },
        close: function () {
            var value = this.input.val();
            if (!value) {
                this.clear();
            } else {
                this.model.save({ text: value });
                this.$el.removeClass("editing");
            }
        },
        updateOnEnter: function (e) {
            if (e.keyCode == 13) this.close();
        },
        clear: function () {
            this.model.destroy();
        }
    });
    $.extend(true, App.Views, { TodoView: todoView });
})(Backbone.View);