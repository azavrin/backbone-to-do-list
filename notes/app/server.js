var
    // Mongoose
    mongoose = require("mongoose"),
    MONGO_ADDR = process.env.MONGO_ADDR || "127.0.0.1",
    MONGO_PORT = parseInt(process.env.MONGO_PORT || 27027, 10),
    Note = {},
    
    // Express
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    PORT = parseInt(process.env.PORT || 3000, 10);

Note.Schema = mongoose.Schema({
    cheked: {
        type: Boolean,
        "default": false
    },
    text: {
        type: String,
        trim: true,
        "default": "*Edit your note!*"
    },
    createdAt: {
        type: Date,
        "default": new Date()
    }
}, {
    id: false,

    // Add virtual fields to data here.
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
});

Note.Schema.virtual("id").get(function () {
    return this.get("_id");
});

Note.Model = mongoose.model("Note", Note.Schema);

var notes = [
    {
        cheked: true,
        text: "bla"
    }, {
        cheked: false,
        text: "bla2"
    }
];


app.use(express.static(__dirname));
app.use(bodyParser.json());

// -----------------------
// Application helpers
// -----------------------
// Generic results handler.
function _handler(res, status) {
    return function (err, results) {
        if (err) {
            return res.status(500).json({ err: err.toString() });
        }

        // Translate to JSON.
        if (Array.isArray(results)) {
            results = results.map(function (m) {
                return m.toJSON();
            });
        } else {
            results = null;
        }

        res.set("cache-control", "no-cache");
        res.status(status || 200).json(results);
    };
}

// Connect to MongoDB.
mongoose.connect("mongodb://" + MONGO_ADDR);

app.get('/notes', function (req, res) { // Get notes list
    Note.Model.find({}, _handler(res));
});

app.post('/notes', function (req, res) { // Create
    Note.Model.create(req.body, _handler(res, 201));
});

app.put("/notes/:id", function (req, res) { // Update
    Note.Model.findByIdAndUpdate(req.params.id, {
        "$set": {
            cheked: req.body.cheked,
            text: req.body.text
        }
    }, _handler(res));
});
app.delete("/notes/:id", function (req, res) { // Delete
    Note.Model.findByIdAndRemove(req.params.id, _handler(res));
});

app.listen(PORT, function () {
    console.log('Server listening on ' + PORT);
});
