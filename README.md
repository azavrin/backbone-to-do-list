# Backbone TO-DO List
> Node/Express + Backbone + MongoDB + Less + Grunt + Bootstrap + Mocha + Chai + Sinon 

Project need installed Grunt, Node.js and MongoDB

## Setup

1. `git clone https://azavrin@bitbucket.org/azavrin/backbone-to-do-list.git`
2. `cd backbone-to-do-list`
3. `npm install`
4. `grunt`

## Usage

1. `mongod` ~~~ run db on first console
2. `npm start` ~~~ run server on second console
3. open browser at http://localhost:3000

## Tests

1. `npm test`