module.exports = function (grunt) {

    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "notes/app/css/style.css": "notes/app/css/style.less",
                }
            }
        },
        codekit: {
            options: {},
            js: {
                files: {
                    'notes/app/js/app-libs-ck.js': 'notes/app/js/libs/app-libs.js'
                }
            },
            testJs: {
                files: {
                    'notes/test/js/test-ck.js': 'notes/test/js/libs/test.js'
                }
            },
            coreLibsJs: {
                files: {
                    'notes/app/js/core-libs-ck.js': 'notes/app/js/libs/core-libs.js'
                }
            },
            testLibsJs: {
                files: {
                    'notes/test/js/test-libs-ck.js': 'notes/test/js/libs/test-libs.js'
                }
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            test: {
                src: ['notes/test/js/test-libs-ck.js', 'notes/app/js/core-libs-ck.js', 'notes/app/js/app-libs-ck.js'],
                dest: 'notes/test/js/test-main-ck.js',
            },
            dist: {
                src: ['notes/app/js/core-libs-ck.js', 'notes/app/js/app-libs-ck.js', 'notes/app/js/app.js'],
                dest: 'notes/app/js/main-ck.js',
            }
        },
        clean: ["notes/app/js/core-libs-ck.js", "notes/app/js/app-libs-ck.js", "notes/test/js/test-libs-ck.js"]
        /*uglify: {
            options: {
                mangle: false
            },
            libs: {
                files: {
                    'notes/app/js/main-ck.min.js': ['notes/app/js/main-ck.js'],
                    'notes/test/js/test-libs-ck.min.js': ['notes/test/js/test-libs-ck.js']
                }
            }
        },*/
    });

    grunt.loadNpmTasks('grunt-codekit');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['less','codekit', 'concat', 'clean'/*, 'uglify'*/]);
};